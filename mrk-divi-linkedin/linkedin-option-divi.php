<?php
/**
 * @package MRK\Divi
 */

/**
 * Plugin Name: LinkedIn-DIVI
 * Version: 1.0.0
 * Description: Adds linked in to Divi's options
 * Author: MRK Dev
 * Author URI: https://www.mrkdevelopment.com/
 * Text Domain: mrk-divi
 * License: GPL v3
 */

/**
 * Loads theme settings
 *
 */
if ( ! function_exists( 'et_load_core_options' ) ) {

	function et_load_core_options() {

		global $shortname, $$themename;
		require_once get_template_directory() . esc_attr( "/options_{$shortname}.php" );
		$newOptions = [];
		foreach ($options as $i => $optionArray) {
			$newOptions[] = $optionArray;
			if (isset($optionArray['id']) && $optionArray['id'] == 'divi_show_google_icon') {

				$showOptions = array( 
					"name" =>esc_html__( "Show Linked In Icon", $themename ),
                   	"id" => $shortname."_show_linkedin_icon",
                   	"type" => "checkbox2",
                   	"std" => "on",
                   	"desc" =>esc_html__( "Here you can choose to display the LINKED IN Icon. ", $themename ) );

				$newOptions[] = $showOptions;
			}

			if (isset($optionArray['id']) && $optionArray['id'] == 'divi_google_url') {

				$urlOptions = array( "name" =>esc_html__( "Linked In Profile Url", $themename ),
		                   "id" => $shortname."_linkedin_url",
		                   "std" => "#",
		                   "type" => "text",
		                   "validation_type" => "url",
						   "desc" =>esc_html__( "Enter the URL of your LinkedIn Profile. ", $themename ) );

				$newOptions[] = $urlOptions;
			}
		}

		$options = $newOptions;
		
	}

}

